using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;

namespace UnitTests.ControllersShould
{

    [TestClass]
    public class GameSessionShould
    {

        private GameSession gameSession = new GameSession();

            [TestMethod]
        public void CreateGameSessionWith6Rounds()
        {
            //Given

            //When
            gameSession.InitializeRounds();
            //Then
            Assert.AreEqual(gameSession.GetRounds().Count, 6);

        }

        [TestMethod]
        public void CreateGameSessionWithPlayerAndOpponent()
        {
            //Given
            gameSession.Starter = new PlayerSession();
            gameSession.Opponent = new PlayerSession();
            //When

            //Then
            Assert.IsNotNull(gameSession.GetStarter());
            Assert.IsNotNull(gameSession.GetOpponent());

        }


        //private GameSession WhenGameSessionIsCreatedAsync(Action OnGameSessionCreated)
        //{
        //    var builder = new ContainerBuilder();
        //    builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
        //        .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
        //    var container = builder.Build();


        //    using (var scope = container.BeginLifetimeScope())
        //    {
        //        IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();
        //        var gameSession = gameSessionApplicationService.CreateGameSessionForUser();

        //        return gameSession;
        //    }

        //}

    }
}
