﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web_Api.Entities;
using Web_Api.Providers;

namespace UnitTests.ControllersShould
{
    [TestClass]
    public class DefineWinnerShould
    {

        #region Attributes
        private GameSession gameSession;
        private PlayerSession starterSession;
        private PlayerSession opponentSession;
        private Round round;
        private Turn starterTurn;
        private Turn opponentTurn;
        private List<WrittenWord> starterWords;
        private List<WrittenWord> opponentWords;
        private int starterOldScore;
        private int opponentOldScore;
        #endregion

        [TestInitialize]
        public void Setup() {
            User starterUser = new User("1", "User1");
            User opponentUser = new User("2", "User2");
            gameSession = new GameSession(starterUser, opponentUser);
            gameSession.InitializeRounds();
            starterSession = gameSession.Starter;
            opponentSession = gameSession.Opponent;
            starterUser.Victories = 5;
        }

        [TestMethod]
        public void ExpectExceptionIsRoundIsNotCompleted() {
            //Given
            SetRoundNumber(1);
            starterTurn.Validated = true;
            opponentTurn.Validated = false;

            Assert.ThrowsException<Exception>(()=> WhenDefineRoundWinner());
        }

        [TestMethod]
        public void GiveVictoryToStarterByWordValidCount() {
            //Given
            SetRoundNumber(1);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            starterWords[0].IsValid = true;

            WhenDefineRoundWinner();

            AssertStarterIsRoundWinner();
        }

        [TestMethod]
        public void GiveVictoryToStarterByTimeLeft() {
            //Given
            SetRoundNumber(1);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            starterWords[0].IsValid = true;
            opponentWords[0].IsValid = true;
            starterTurn.TimeLeft = 50;
            opponentTurn.TimeLeft = 40;

            WhenDefineRoundWinner();

            AssertStarterIsRoundWinner();
        }

        [TestMethod]
        public void GiveVictoryToOpponentByWordValidCount() {
            //Given
            SetRoundNumber(1);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            opponentWords[0].IsValid = true;

            WhenDefineRoundWinner();

            AssertOpponentIsWinner();
        }

        [TestMethod]
        public void GiveVictoryToOpponentByTimeLeft() {
            //Given
            SetRoundNumber(1);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            starterWords[0].IsValid = true;
            opponentWords[0].IsValid = true;
            starterTurn.TimeLeft = 40;
            opponentTurn.TimeLeft = 50;

            WhenDefineRoundWinner();

            AssertOpponentIsWinner();
        }

        [TestMethod]
        public void GiveVictoryScoreToStarterOnSecondRound() {
            //Given
            starterSession.Score = 1;
            SetRoundNumber(2);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            starterTurn.WordsInputList[0].IsValid = true;

            WhenDefineRoundWinner();

            AssertStarterIsRoundWinner();
        }

        [TestMethod]
        public void GiveVictoryScoreToOpponentOnSecondRound() {
            //Given
            starterSession.Score = 1;
            SetRoundNumber(2);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            opponentTurn.WordsInputList[0].IsValid = true;

            WhenDefineRoundWinner();

            AssertOpponentIsWinner();
        }

        [TestMethod]
        public void FinalizeGameIfPlayerReachedMaxWins() {
            GivenStarterWithEnoughScoreToWinGameSession();

            WhenDefineRoundWinner();

            Assert.IsTrue(gameSession.CurrentState == GameSession.GameSessionState.FinalizedGame);
            AssertStarterIsRoundWinner();
            AssertStarterIsGameSessionWinner();
        }

        [TestMethod]
        public void UpdateWinnerVictoryCount() {
            GivenStarterWithEnoughScoreToWinGameSession();
            int starterPreviousVictories = starterSession.User.Victories;

            WhenDefineRoundWinner();

            Assert.IsTrue(starterSession.User.Victories== starterPreviousVictories+1);
        }

        #region Shared methods

        private void SetRoundNumber(int roundNumber) {
            round = gameSession.GetRound(roundNumber);
            bool turn0IsStarter = round.Number % 2 != 0;
            starterTurn = round.Turns[turn0IsStarter ? 0 : 1];
            opponentTurn = round.Turns[turn0IsStarter ? 1 : 0];
            starterWords = starterTurn.WordsInputList;
            opponentWords = opponentTurn.WordsInputList;
        }

        private void GivenStarterWithEnoughScoreToWinGameSession() {
            starterSession.Score = GameSession.MAX_WINS - 1;
            SetRoundNumber(GameSession.MAX_WINS + 1);
            starterTurn.Validated = true;
            opponentTurn.Validated = true;
            starterTurn.WordsInputList[0].IsValid = true;
        }

        private void WhenDefineRoundWinner() {
            starterOldScore = starterSession.Score;
            opponentOldScore = opponentSession.Score;
            DefineWinnerApplicationService.DefineRoundWinner(gameSession, round);
        }

        private void AssertStarterIsRoundWinner() {
            Assert.IsTrue(starterSession.Score-starterOldScore > opponentSession.Score-opponentOldScore);
        }

        private void AssertOpponentIsWinner() {
            Assert.IsTrue(starterSession.Score-starterOldScore < opponentSession.Score-opponentOldScore);
        }

        private void AssertStarterIsGameSessionWinner() {
            Assert.IsTrue(starterSession.Score>= GameSession.MAX_WINS &&
                          starterSession.Score>opponentSession.Score);
        }
        #endregion
    }
}
