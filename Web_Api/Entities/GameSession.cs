using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Web_Api.Entities
{
    [Serializable]
    public class GameSession : Entity
    {
        private List<Round> rounds;
        public List<Round> Rounds
        {
            get => rounds;
            set => rounds = value;
        }

        private PlayerSession starter;
        public PlayerSession Starter
        {
            get => starter;
            set => starter = value;
        }

        private PlayerSession opponent;
        public PlayerSession Opponent
        {
            get => opponent;
            set => opponent = value;
        }

        [NonSerialized]
        private Round currentRound;

        private int _roundNumber;

        public int RoundNumber {
            get => _roundNumber;
            set => _roundNumber = value;
        }

        [NonSerialized]
        private const int MAX_NUMBER_ROUNDS = 6;
        [NonSerialized]
        public const int MAX_WINS = 3;

        public enum GameSessionState { StarterTurn, OpponentTurn, FinalizedGame }

        private GameSessionState _gameSessionState = GameSessionState.StarterTurn;

        public GameSessionState CurrentState {
            get => _gameSessionState;
            set => _gameSessionState = value;
        }

        public GameSession() { }

        public GameSession(PlayerSession starter, PlayerSession opponent) : this() {
            this.starter = starter;
            this.opponent = opponent;
        }

        public GameSession(User starter, User opponent) : this() {
            this.starter = new PlayerSession(starter);
            this.opponent = new PlayerSession(opponent);
        }

        public void InitializeRounds() {

            rounds = new List<Round>();

            for (int i = 0; i < MAX_NUMBER_ROUNDS; i++) {
                Round newRound = new Round(i + 1);

                foreach (var item in newRound.Turns) {
                    item.InitializeWordInputList();
                }

                this.rounds.Add(newRound);
            }
            currentRound = rounds[0];
            _roundNumber = currentRound.Number;
        }

        public Round GetRound(int roundNumber) {
            return rounds.Find(round => round.Number == roundNumber);
        }

        public Round GetPreviousRound() {
            return Rounds.Find(round => round.Number == currentRound.Number - 1);
        }

        public Round GetNextRound() {
            return Rounds.Find(round => round.Number == currentRound.Number + 1);
        }

        public void SetCurrentRound(Round localRound) {
            for (int i = 0; i < MAX_NUMBER_ROUNDS; i++) {
                if (rounds[i].Number == localRound.Number) {
                    rounds[i] = localRound;
                    currentRound = localRound;
                    _roundNumber = localRound.Number;
                    return;
                }
            }
        }

        public List<Round> GetRounds() {
            return this.rounds;
        }

        public Round GetCurrentRound() {
            return currentRound;
        }

        public PlayerSession GetStarter() {
            return this.starter;
        }

        public PlayerSession GetOpponent() {
            return this.opponent;
        }

        public bool IsStarterPlayer(string id) {
            if (id == this.starter.GetUser().Id) {
                return true;
            }

            return false;
        }

        public bool ContainsPlayer(string id) {
            return this.starter.User.Id == id || opponent.User.Id == id;
        }

        public bool IsNewGame()
        {
            return GetCurrentRound() == GetRounds()[0] &&
               string.IsNullOrWhiteSpace(GetCurrentRound().Letter);
        }
        public PlayerSession GetOppositePlayer(PlayerSession player) {
            return GetOppositePlayer(player.User.Id);
        }

        public PlayerSession GetOppositePlayer(string id) {
            if (IsStarterPlayer(id)) {
                return opponent;
            } else {
                return starter;
            }
        }

        public bool HasReachedFinalScore() {
            return Starter.Score >= MAX_WINS || Opponent.Score >= MAX_WINS;
        }

        public PlayerSession GetWinner() {
            if (HasReachedFinalScore()) {
                return Starter.Score >= MAX_WINS ? Starter : Opponent;
            }
            return null;
        }

        public void FinalizeGame() {
            if (HasReachedFinalScore()) {
                _gameSessionState = GameSessionState.FinalizedGame;
                GetWinner().User.Victories++;
            } else {
                throw new Exception($"Score in game session {Id} is not enough to finalize game " +
                                    $"(starter-{starter.Score},opponent-{opponent.Score})");
            }
        }

        [OnDeserialized]
        public void AfterDeserialize(StreamingContext context) {
            currentRound = rounds.Find(round => round.Number == _roundNumber);
        }
    }
}

