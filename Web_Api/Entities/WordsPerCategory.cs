﻿using System.Collections.Generic;

namespace Web_Api.Entities
{
	public class WordsPerCategory : Entity
	{

		private string categoryName;
		public string CategoryName {
			get => categoryName;
			set => categoryName = value;
		}

		private List<string> words;
		public List<string> Words {
			get => words;
			set => words = value;
		}

		public WordsPerCategory() { }
	}
}