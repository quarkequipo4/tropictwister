
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Web_Api.Entities
{
	[Serializable]
	public class Category : Entity
	{
		private string categoryName;
		public string CategoryName {
			get => categoryName;
			set => categoryName = value;
		}

		public Category() { }

	}
}