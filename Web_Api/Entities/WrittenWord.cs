
using Web_Api.Entities;
using System;

namespace Web_Api.Entities
{
	[Serializable]
	public class WrittenWord : Entity
	{


		private bool isValid;
		public bool IsValid
		{
			get => isValid;
			set => isValid = value;
		}

		private string text;
		public string Text
		{
			get => text;
			set => text = value;
		}

		private Category category;
		public Category Category
		{
			get => category;
			set => category = value;
		}

		public WrittenWord() { }

		public WrittenWord(string text, Category category)
		{
			this.text = text;
			this.category = category;
		}

		public string GetText()
		{
			return text;
		}

		public void SetText(string text)
		{
			this.text = text;
		}

		public Category GetCategory()
		{
			return category;
		}

		public void SetCategory(Category category)
		{
			this.category = category;
		}
	}
}