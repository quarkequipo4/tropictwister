

using Web_Api.Entities;
using System;
using System.Collections.Generic;

namespace Web_Api.Entities
{
	[Serializable]
	public class User : Entity
	{


		private string name;
		public string Name
		{
			get => name;
			set => name = value;
		}
		private Wallet wallet;
		public Wallet Wallet
		{
			get => wallet;
			set => wallet = value;
		}
		private List<PowerUp> powerUps;
		public List<PowerUp> PowerUps
		{
			get => powerUps;
			set => powerUps = value;
		}
		[NonSerialized]
		private List<GameSession> gameSessions;
		public List<GameSession> GameSessions
		{
			get => gameSessions;
			set => gameSessions = value;
		}

		private Uri picturePath;
		public Uri PicturePath
		{
			get => picturePath;
			set => picturePath = value;
		}

		private int victories;

		public int Victories
        {
			get => victories;
			set => victories = value;
        }

		public User() { }

		public User(string id, string name)
		{
			this.Id = id;
			this.name = name;
		}
		public User(string id, string name, Uri profilePhoto)
		{
			this.Id = id;
			this.name = name;
			this.picturePath = profilePhoto;
		}

		public Uri GetPicturePath()
		{
			return picturePath;
		}
	}
}