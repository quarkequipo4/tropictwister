﻿using System;
using System.Collections;
using System.Collections.Generic;
using Web_Api.Enums;

public class RandomLetter
{
    Letters letters;

    public string letter { get; }

    public RandomLetter()
    {
        Array values = Enum.GetValues(typeof(Letters));

        Random random = new Random();
        var randomNumber = (Letters)values.GetValue(random.Next(values.Length));

        letter = randomNumber.ToString();
    }


    //public string GetLetter() 
    //{
    //    string[] availableLetters = { "A", "P", "D", "M", "T" };
    //    var randomNumber = new Random(availableLetters.Length).Next();
        
    //    return availableLetters[randomNumber];
    //}
}
