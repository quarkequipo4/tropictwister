
using System.Collections.Generic;
using System;
using System.Linq;

namespace Web_Api.Entities
{
	[Serializable]
	public class Round
	{


		private int number;
		public int Number
		{
			get => number;
			set => number = value;
		}

		private string letter;
		public string Letter
		{
			get => letter;
			set => letter = value;
		}

		private List<Category> categories;
		public List<Category> Categories
		{
			get => categories;
			set => categories = value;
		}

		private List<Turn> turns;
		public List<Turn> Turns
		{
			get => turns;
			set => turns = value;
		}

		public Round() { }

		public Round(int number)
		{
			this.number = number;

			turns = new List<Turn>();

			for (int i = 0; i < 2; i++)
			{
				Turn newTurn = new Turn();

				this.turns.Add(newTurn);
			}
		}

		public virtual List<Category> GetCategories()
		{
			return categories;
		}

		public int GetNumber()
		{
			return number;
		}

		public void SetCategories(List<Category> categories)
		{
			this.categories = categories;
			for (int i = 0; i < categories.Count; i++)
			{
				turns.ForEach(round => round.GetWordInputList()[i].SetCategory(categories[i]));
			}
		}

		public bool IsNewRound() {
			return !turns[0].Validated && !turns[1].Validated;
		}

		public bool IsFirstRound() {
			return turns[0].Validated && !turns[1].Validated;
		}

		public bool IsRoundCompleted() {
			return turns.All(item => item.IsValidated());
		}

		public bool HasLetterDefined() {
			return !string.IsNullOrEmpty(letter);
		}
	}
}