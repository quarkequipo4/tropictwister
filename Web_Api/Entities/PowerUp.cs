namespace Web_Api.Entities
{
	public class PowerUp
	{

		private string name;
		public string Name
		{
			get => name;
			set => name = value;
		}
		private int cost;
		public int Cost
		{
			get => cost;
			set => cost = value;
		}

		public PowerUp() { }

		public string GetName()
		{
			return "";
		}

		public int GetCost()
		{
			return 0;
		}
	}
}