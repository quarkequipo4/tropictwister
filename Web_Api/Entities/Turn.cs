using System;
using System.Collections.Generic;

namespace Web_Api.Entities
{
	[Serializable]
	public class Turn
	{


		private List<WrittenWord> wordInputList = new List<WrittenWord>();
		public List<WrittenWord> WordsInputList
		{
			get => wordInputList;
			set => wordInputList = value;
		}

		private bool validated;
		public bool Validated
		{
			get => validated;
			set => validated = value;
		}

		private float timeLeft;
		public float TimeLeft
		{
			get => timeLeft;
			set => timeLeft = value;
		}

		public Turn() { }

		public List<WrittenWord> GetWordInputList()
		{
			return wordInputList;
		}


		public void InitializeWordInputList()
		{

			for (int i = 0; i < 5; i++)
			{
				wordInputList.Add(new WrittenWord());
			}

		}
		public void SetValidated(bool validated)
		{
			this.validated = validated;
		}
		public bool IsValidated()
		{
			return validated;
		}

		public void SetWords(string[] words)
		{
			for (int i = 0; i < 5; i++)
			{
				wordInputList[i].SetText(words[i]);
			}
		}
	}
}