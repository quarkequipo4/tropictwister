
using System;

namespace Web_Api.Entities
{
	[Serializable]
	public class Wallet
	{

		private int coins;
		public int Coins
		{
			get => coins;
			set => coins = value;
		}

		public Wallet() { }

		public void AddCoins(int coinAmount)
		{
			this.coins += coinAmount;
		}

		public int GetCoins()
		{
			return coins;
		}

		public void RemoveCoins(int coinAmount)
		{
			this.coins -= coinAmount;
		}
	}
}