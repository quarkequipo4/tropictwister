﻿namespace Web_Api.Entities
{
    public class Entity
    {
        private string id;
        public string Id {
            get => id;
            set => id = value;
        }

        public override bool Equals(object other) {
            if (other.GetType()==GetType()) {
                Entity otherEntity = (Entity)other;
                return otherEntity.Id.Equals(Id);
            } else {
                return false;
            }
        }
    }
}
