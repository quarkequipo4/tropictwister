
using System;

namespace Web_Api.Entities
{
	[Serializable]
	public class PlayerSession
	{

		private int score;
		public int Score
		{
			get => score;
			set => score = value;
		}

		private User user;
		public User User
		{
			get => user;
			set => user = value;
		}

		private bool isMyTurn;
		public bool IsMyTurn
		{
			get => isMyTurn;
			set => isMyTurn = value;
		}

		public PlayerSession() { }

		public PlayerSession(User user)
		{
			this.user = user;
		}

		public int GetScore()
		{
			return score;
		}

		public User GetUser()
		{
			return user;
		}

		public void SetScore(int score)
		{
			this.score = score;
		}

		public void SetTurn(bool isMyTurn)
		{
			this.isMyTurn = isMyTurn;
		}

		public string GetName()
		{
			return user.Name;
		}
	}
}