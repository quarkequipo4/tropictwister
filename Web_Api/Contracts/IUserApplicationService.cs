﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_Api.Entities;

namespace Web_Api.Contracts
{
    public interface IUserApplicationService : IApplicationService<User, string>
    {
    }
}
