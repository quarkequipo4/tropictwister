﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api.Contracts
{
    public interface IApplicationService
    {
        
    }

    public interface IApplicationService<TEntity, TId> : IApplicationService where TEntity : class
    {
        Task<List<TEntity>> GetAllAsync();

        Task<TEntity> GetByIdAsync(TId id);

        Task Create(TEntity entity);

        Task Update(TId id, TEntity entity);

        Task Delete(TId id);
    }
}
