﻿using System.Threading.Tasks;
using Web_Api.Entities;

namespace Web_Api.Contracts
{
    public interface IValidationApplicationService
    {
        Task<Turn> Validate(string gameSessionId);
    }
}