﻿using System.Threading.Tasks;
using Web_Api.Entities;
namespace Web_Api.Contracts
{
    public interface IDefineWinnerApplicationService
    {
        Task<GameSession> DefineWinner(string gameSessionId, int roundNumber);
    }
}