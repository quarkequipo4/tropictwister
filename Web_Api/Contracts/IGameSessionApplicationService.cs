﻿using Web_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api.Contracts
{
    public interface IGameSessionApplicationService : IApplicationService<GameSession, string>
    {
        //Task<GameSession> CreateGameSessionForUser(string id);

        Task<GameSession> CreateGameSessionForUser(string id);
        Task<List<GameSession>> GetAll(string userId);
        Task<GameSession> UpdateRoundForGameSession(string gameSessionId, Round round);
        Task<GameSession> FinalizeGameSession(string gameSessionId);

    }
}
