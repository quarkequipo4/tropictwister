﻿using Assets.Scripts.DataAccess.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;

namespace Web_Api.Providers
{
    public class ValidationApplicationService : ApplicationService<GameSession, string>, IValidationApplicationService
    {

        Dictionary<string, List<string>> wordsInCategories;

        public ValidationApplicationService(IServiceRepository<GameSession, string> repository) : base(repository) {

        }

        public async Task<Turn> Validate(string gameSessionId) {

            await InitializeWordsInCategories();

            GameSession gameSession = await GetByIdAsync(gameSessionId);
            Round currentRound = gameSession.GetCurrentRound();
            Turn currentTurn;
            if (!currentRound.Turns[0].IsValidated()) {
                currentTurn = currentRound.Turns[0];
            } else {
                currentTurn = currentRound.Turns[1];
            }
            ValidateMatches(currentRound.Categories, currentRound.Letter, currentTurn);

            await Update(gameSessionId, gameSession);

            return currentTurn;
        }

        #region Validate words private methods

        private async Task InitializeWordsInCategories() {

            var repository = FactoryRepository.Instance.GetRepository<WordsPerCategory, string>();
            List<WordsPerCategory> categoriesServ = await repository.GetAll();

            wordsInCategories = new Dictionary<string, List<string>>();
            foreach (WordsPerCategory category in categoriesServ) {
                wordsInCategories.Add(category.CategoryName, new List<string>(category.Words));
            }
        }

        private void ValidateMatches(List<Category> categories, string letter, Turn turn) {
            List<WrittenWord> writtenWords = turn.GetWordInputList();
            bool[] validations = CalculateScore(categories, letter, writtenWords);
            for (int i = 0; i < writtenWords.Count; i++) {
                writtenWords[i].IsValid = validations[i];
            }
            turn.SetValidated(true);
        }

        private bool[] CalculateScore(List<Category> roundCategories, string letter, List<WrittenWord> writtenWords) {
            bool[] score = new bool[5];

            for (int i = 0; i < roundCategories.Count; i++) {

                if (!CheckLetter(writtenWords[i], letter)) continue;

                List<string> wordsInCategory = wordsInCategories[roundCategories[i].CategoryName];
                if (wordsInCategory.Contains(writtenWords[i].GetText()))
                    score[i] = true;
            }
            return score;
        }

        private bool CheckLetter(WrittenWord writtenWord, string letter) {
            if (!string.IsNullOrEmpty(writtenWord.GetText())) {
                string firstLetter = writtenWord.GetText()[0].ToString();
                bool result = firstLetter == letter;
                return result;
            } else return false;
        }
        #endregion

    }
}