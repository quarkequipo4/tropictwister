﻿using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts.DataAccess.Repositories;
using Web_Api.Utils;

namespace Web_Api.Providers
{
    public class CategoryApplicationService : ApplicationService<Category, string>, ICategoryApplicationService
    {
        IServiceRepository<GameSession, string> gameSessionRepository;
        private GameSession currentGameSession;

        public CategoryApplicationService(IServiceRepository<Category, string> repository): base(repository)
        {
            gameSessionRepository = FactoryRepository.Instance.GetRepository<GameSession, string>();
        }

        public async Task<List<Category>> DefineNewCategories(string gameSessionId) {
            currentGameSession = await gameSessionRepository.Get(gameSessionId);
            List<Category> categories = await GetAllAsync();
            ShuffleCategories(categories);
            List<Category> selectedCategories = categories.Take(5).ToList();
            await SaveCategories(selectedCategories);
            return selectedCategories;
        }

        private void ShuffleCategories(List<Category> categories) {
            categories.Shuffle<Category>();
        }

        private async Task SaveCategories(List<Category> categories) {
            currentGameSession.GetCurrentRound().SetCategories(categories);
            await gameSessionRepository.Update(currentGameSession);
        }
    }
}
