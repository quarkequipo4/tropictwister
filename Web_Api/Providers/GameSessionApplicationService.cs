﻿using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts.DataAccess.Repositories;

namespace Web_Api.Providers
{
    public class GameSessionApplicationService : ApplicationService<GameSession, string>, IGameSessionApplicationService {
        public GameSessionApplicationService(IServiceRepository<GameSession, string> repository) : base(repository) {

        }

        //public Task<GameSession> CreateGameSessionForUser(string id)
        //{
        //    GameSession gameSession = new GameSession();
        //    gameSession.InitializeRounds();

        //    return ;
        //}

        public async Task<GameSession> CreateGameSessionForUser(string id) {
            List<User> users = await FactoryRepository.Instance.GetRepository<User, string>().GetAll();
            PlayerSession playerSession, opponentSession;
            List<User> usersWithoutSelf = new List<User>(users);

            User playerUser = users.Find(user => user.Id == id);
            usersWithoutSelf.Remove(playerUser);
            Random random = new Random();

            User opponentUser = Matchmaking(usersWithoutSelf, playerUser);

            playerSession = new PlayerSession(playerUser);
            opponentSession = new PlayerSession(opponentUser);

            GameSession gameSession = new GameSession(playerSession, opponentSession);
            gameSession.InitializeRounds();

            await FactoryRepository.Instance.GetRepository<GameSession, string>().Add(gameSession);

            return gameSession;
        }

        private User Matchmaking(List<User> users, User playerUser) {
            List<User> opponents = users.Where(c => Enumerable.Range(-5, 5*2).Contains(c.Victories - playerUser.Victories)).ToList();

            if (opponents.Count() == 0) {
                opponents = users.Where(c => Enumerable.Range(-30, 30*2).Contains(c.Victories - playerUser.Victories)).ToList();
            }

            Random random = new Random();

            return opponents.Count > 0 ? opponents[random.Next(opponents.Count)] : users[random.Next(users.Count)];
        }

        public async Task<List<GameSession>> GetAll(string userId) {
            List<GameSession> allGameSessions = await GetAllAsync();
            return allGameSessions.FindAll(x => x.ContainsPlayer(userId));
        }

        public async Task<GameSession> UpdateRoundForGameSession(string gameSessionId, Round round) {
            GameSession gameSession = await GetByIdAsync(gameSessionId);
            ManageTurnOrRound(round, gameSession);
            await Update(gameSessionId, gameSession);
            return gameSession;
        }

        public async Task<GameSession> FinalizeGameSession(string gameSessionId)
        {
            GameSession gameSession = await GetByIdAsync(gameSessionId);

            gameSession = RemoveUnusedRounds(gameSession);

            await Update(gameSessionId, gameSession);

            return gameSession;

        }

        public static GameSession RemoveUnusedRounds(GameSession gameSession)
        {
            Round currentRound = gameSession.GetCurrentRound();

            List<Round> newList = new List<Round>();

            foreach (Round round in gameSession.Rounds)
            {
                if (round == currentRound) newList.Add(round);
            }

            gameSession.Rounds = newList;

            return gameSession;
        }

        public static void ManageTurnOrRound(Round localRound, GameSession currentGameSession) {
            currentGameSession.SetCurrentRound(localRound);
            var allValidated = localRound.IsRoundCompleted();

            if (allValidated) {
                ChangeToNextRound(currentGameSession);
            } else {
                ChangeTurn(localRound, currentGameSession);
            }
        }

        private static void ChangeTurn(Round localRound, GameSession gameSession) {
            if (!localRound.IsNewRound()) {
                if (localRound.Number % 2 != 0) {
                    gameSession.CurrentState = GameSession.GameSessionState.OpponentTurn;
                } else {
                    gameSession.CurrentState = GameSession.GameSessionState.StarterTurn;
                }
            } else {
                gameSession.CurrentState = gameSession.CurrentState;
            }
        }

        private static void ChangeToNextRound(GameSession gameSession) {
            gameSession.SetCurrentRound(gameSession.GetNextRound());
        }
    }
}
