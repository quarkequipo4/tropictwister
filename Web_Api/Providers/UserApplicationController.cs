﻿using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts.DataAccess.Repositories;

namespace Web_Api.Providers
{
    public class UserApplicationService : ApplicationService<User, string>, IUserApplicationService
    {
        public UserApplicationService(IServiceRepository<User, string> repository) : base(repository)
        {

        }

    }
}
