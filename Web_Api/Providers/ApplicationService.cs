﻿using Autofac;
using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api.Providers
{
    public class ApplicationService
    {
    }

    public class ApplicationService<TEntity, TId> : ApplicationService, IApplicationService<TEntity, TId> where TEntity : class
    {

        protected IServiceRepository<TEntity, TId> repository;

        protected IComponentContext context;
        private IComponentContext Context
        {
            get
            {
                return this.context;
            }
        }
        public ApplicationService(IServiceRepository<TEntity, TId> repository)
        {
            this.repository = repository;
        }
        public ApplicationService(IServiceRepository<TEntity, TId> repository, IComponentContext context)
            : this(repository)
        {

            this.context = context;
        }

    

        protected T ResolveService<T>()
        {
            return this.Context.Resolve<T>();
        }
        
        public virtual async Task Create(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(string.Format("La entidad de tipo {0} no puede ser null", nameof(entity)));

             await this.repository.Add(entity);
        }

        public virtual async Task Delete(TId id)
        {
            var entity = await this.repository.Get(id);

            if (entity == null)
                throw new ArgumentNullException(string.Format("No se encontró la entidad de tipo {0} con el Id {1}", typeof(TEntity).Name, id));

            //No esta implementado el Remove
            await this.repository.Remove(entity);
        }

        public virtual async Task<List<TEntity>> GetAllAsync()
        {
            var entities = await this.repository.GetAll();

            if (entities == null)
                throw new ArgumentNullException(string.Format("No se encontraron las entidades de tipo {0}", typeof(TEntity).Name));

            return entities;
        }

        public virtual async Task<TEntity> GetByIdAsync(TId id)
        {
            var entity = await this.repository.Get(id);

            if (entity == null)
                throw new ArgumentNullException(string.Format("No se encontró la entidad de tipo {0} con el Id {1}", typeof(TEntity).Name, id));

            return entity;
        }

        public virtual async Task Update(TId id, TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(string.Format("La entidad de tipo {0} no puede ser null", nameof(entity)));

            await this.repository.Update(entity);
        }
    }
}
