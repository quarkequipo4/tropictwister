﻿using Assets.Scripts.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web_Api.Contracts;
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;

namespace Web_Api.Providers
{
    public class DefineWinnerApplicationService : ApplicationService<GameSession, string>, IDefineWinnerApplicationService
    {

        public DefineWinnerApplicationService(IServiceRepository<GameSession, string> repository) : base(repository) {

        }

        public async Task<GameSession> DefineWinner(string gameSessionId, int roundNumber) {
            var userRepository = FactoryRepository.Instance.GetRepository<User, string>();
            GameSession gameSession = await GetByIdAsync(gameSessionId);
            //to make sure it has the users updated
            User starterUser = await userRepository.Get(gameSession.Starter.User.Id);
            User opponentUser = await userRepository.Get(gameSession.Opponent.User.Id);
            Round round = gameSession.GetRound(roundNumber);
            gameSession.Starter.User = starterUser;
            gameSession.Opponent.User = opponentUser;

            DefineRoundWinner(gameSession, round);

            GameSessionApplicationService gameSessionApplicationService = new GameSessionApplicationService(repository);
            await gameSessionApplicationService.Update(gameSession.Id, gameSession);

            if (gameSession.CurrentState == GameSession.GameSessionState.FinalizedGame) {
                await userRepository.Update(gameSession.GetWinner().User);
            }

            return gameSession;
        }

        #region Define winner methods
        public static void DefineRoundWinner(GameSession gameSession, Round round) {
            if (round.IsRoundCompleted()) {

                bool turn0IsStarter = round.Number % 2 != 0;
                Turn starterTurn = round.Turns[turn0IsStarter? 0 : 1],
                       opponentTurn = round.Turns[turn0IsStarter ? 1 : 0];
                bool starterWon = IsStarterPlayerWinner(starterTurn, opponentTurn);
                PlayerSession playerSession = gameSession.Starter,
                              opponentSession = gameSession.Opponent;
                int playerNewScore = playerSession.GetScore() + (starterWon ? 1 : 0);
                int opponentNewScore = opponentSession.GetScore() + (starterWon ? 0 : 1);
                playerSession.SetScore(playerNewScore);
                opponentSession.SetScore(opponentNewScore);

                if (gameSession.HasReachedFinalScore()) {
                    gameSession.FinalizeGame();
                    gameSession.SetCurrentRound(round); //since the current round is set to the next one after word validation
                    GameSessionApplicationService.RemoveUnusedRounds(gameSession);
                }

            } else {
                throw new Exception("Current round is not completed yet");
            }
        }


        #region Define winner private methods
        private static bool IsStarterPlayerWinner(Turn starterTurn, Turn opponentTurn) {
            List<WrittenWord> writtenWordsStarter = starterTurn.WordsInputList;
            List<WrittenWord> writtenWordsOpponent = opponentTurn.WordsInputList;


            int starterTurnScore = writtenWordsStarter.FindAll(writtenWord => writtenWord.IsValid).Count;
            int opponentTurnScore = writtenWordsOpponent.FindAll(writtenWord => writtenWord.IsValid).Count;
            if (starterTurnScore == opponentTurnScore)
                starterTurnScore = starterTurn.TimeLeft > opponentTurn.TimeLeft ? ++starterTurnScore : --starterTurnScore;

            return starterTurnScore > opponentTurnScore;
        }

        
        #endregion

        #endregion


        
    }
}
