﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Web_Api.Controllers
{
    [Route("api/randomLetter")]
    [ApiController]
    public class RandomLetterController : ControllerBase
    {
        //private string GetLetter()
        //{
        //    return localRound.IsNewRound() ? randomLetter.SelectedLetter() : localRound.Letter;
        //}

        //public void SelectLetter(Action selectedCompleted)
        //{
        //    selectedLetter = GetLetter();
        //    localRound.Letter = selectedLetter;
        //    RoundService.SaveLocalRound(localRound, selectedCompleted);
        //}

        [HttpGet]
        public ActionResult<string> Get()
        {
            RandomLetter randLetter = new RandomLetter();

            string letter = randLetter.letter;

            return letter;
        }
    }
}
