﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web_Api.Controllers
{
    [Route("api/gameSession")]
    [ApiController]
    public class GameSessionController : ControllerBase
    {
        //TODO Refactorizar para que el builder sea global

        // GET api/<GameSessionController>/5
        [HttpGet]
        public async Task<ActionResult<GameSession>> Get(string id)
        {

            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();
                var gameSession = await gameSessionApplicationService.GetByIdAsync(id);


                return gameSession;
            }
        }

        // GET api/<GameSessionController>
        [Route("all/{userId}")]
        [HttpGet]
        public async Task<ActionResult<List<GameSession>>> GetAll(string userId) {

            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope()) {
                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();
                var gameSessions = await gameSessionApplicationService.GetAll(userId);


                return gameSessions;
            }
        }

        // POST api/<GameSessionController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] GameSession gameSession)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                await gameSessionApplicationService.Create(gameSession);

                return Ok();
            }
        }

        //// PUT api/<GameSessionController>/5
        [HttpPut]
        public async Task<ActionResult> Put(string id, [FromBody] GameSession gameSession)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                await gameSessionApplicationService.Update(id, gameSession);

                return Ok();
            }
        }

        //// DELETE api/<GameSessionController>/5
        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {


                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                await gameSessionApplicationService.Delete(id);

                return Ok();
            }
        }

        [HttpPost("create/{userId}")]
        public async Task<ActionResult> CreateGameSessionForUser(string userId)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {

                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                var result = await gameSessionApplicationService.CreateGameSessionForUser(userId);

                return Ok(result);
            }
        }

        //// PUT api/<GameSessionController>/5
        [HttpPut]
        [Route("updateRound/{gameSessionId}")]
        public async Task<ActionResult<GameSession>> UpdateRoundForGameSession(string gameSessionId, [FromBody] Round round) {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope()) {

                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                var result = await gameSessionApplicationService.UpdateRoundForGameSession(gameSessionId, round);

                return Ok(result);
            }
        }

        //// DELETE api/<GameSessionController>/5
        [HttpPut]
        [Route("finalizeGameSession/{gameSessionId}")]
        public async Task<ActionResult> FinalizeGameSession(string gameSessionId)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GameSessionApplicationService>().As<IGameSessionApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {


                IGameSessionApplicationService gameSessionApplicationService = scope.Resolve<IGameSessionApplicationService>();

                var result = await gameSessionApplicationService.FinalizeGameSession(gameSessionId);

                return Ok();
            }
        }
    }
}
