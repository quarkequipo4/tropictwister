﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;


namespace Web_Api.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //TODO Refactorizar para que el builder sea global

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(string id)
        {

            var builder = new ContainerBuilder();
            builder.RegisterType<UserApplicationService>().As<IUserApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<User, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IUserApplicationService userApplicationService = scope.Resolve<IUserApplicationService>();
                var user = await userApplicationService.GetByIdAsync(id);


                return user;
            }
        }

        // GET api/<UserController>/5
        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {

            var builder = new ContainerBuilder();
            builder.RegisterType<UserApplicationService>().As<IUserApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<User, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IUserApplicationService userApplicationService = scope.Resolve<IUserApplicationService>();
                var user = await userApplicationService.GetAllAsync();


                return user;
            }
        }

        // POST api/<UserController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] User user)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UserApplicationService>().As<IUserApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<User, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IUserApplicationService userApplicationService = scope.Resolve<IUserApplicationService>();

                await userApplicationService.Create(user);

                return Ok();
            }
        }

        //// PUT api/<UserController>/5
        [HttpPut]
        public async Task<ActionResult> Put(string id, [FromBody] User user)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UserApplicationService>().As<IUserApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<User, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                IUserApplicationService userApplicationService = scope.Resolve<IUserApplicationService>();

                await userApplicationService.Update(id, user);

                return Ok();
            }
        }

        //// DELETE api/<UserController>/5
        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<UserApplicationService>().As<IUserApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<User, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {


                IUserApplicationService userApplicationService = scope.Resolve<IUserApplicationService>();

                await userApplicationService.Delete(id);

                return Ok();
            }
        }
    }
}
