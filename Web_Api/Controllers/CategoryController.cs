﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Web_Api.Enums;


namespace Web_Api.Controllers
{
        [Route("api/category")]
        [ApiController]
        public class CategoryController : ControllerBase
        {
            //TODO Refactorizar para que el builder sea global

            // GET api/<CategoryController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> Get(string id)
            {

                var builder = new ContainerBuilder();
                builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                    .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
                var container = builder.Build();


                using (var scope = container.BeginLifetimeScope())
                {
                    ICategoryApplicationService categoryApplicationService = scope.Resolve<ICategoryApplicationService>();
                    var category = await categoryApplicationService.GetByIdAsync(id);


                    return category;
                }
            }

        // GET api/<CategoryController>/5
        [HttpGet]
        public async Task<ActionResult<List<Category>>> Get()
        {

            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
            var container = builder.Build();


            using (var scope = container.BeginLifetimeScope())
            {
                ICategoryApplicationService categoryApplicationService = scope.Resolve<ICategoryApplicationService>();
                var category = await categoryApplicationService.GetAllAsync();


                return category;
            }
        }

        // POST api/<CategoryController>
        [HttpPost]
            public async Task<ActionResult> Post([FromBody] Category category)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                    .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
                var container = builder.Build();


                using (var scope = container.BeginLifetimeScope())
                {
                    ICategoryApplicationService categoryApplicationService = scope.Resolve<ICategoryApplicationService>();

                    await categoryApplicationService.Create(category);

                    return Ok();
                }
            }

            //// PUT api/<CategoryController>/5
            [HttpPut]
            public async Task<ActionResult> Put(string id, [FromBody] Category category)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                    .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
                var container = builder.Build();


                using (var scope = container.BeginLifetimeScope())
                {
                    ICategoryApplicationService categoryApplicationService = scope.Resolve<ICategoryApplicationService>();

                    await categoryApplicationService.Update(id, category);

                    return Ok();
                }
            }

            //// DELETE api/<CategoryController>/5
            [HttpDelete]
            public async Task<ActionResult> Delete(string id)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                    .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
                var container = builder.Build();


                using (var scope = container.BeginLifetimeScope())
                {


                    ICategoryApplicationService categoryApplicationService = scope.Resolve<ICategoryApplicationService>();

                    await categoryApplicationService.Delete(id);

                    return Ok();
                }
            }

            [Route("define/{gameSessionId}")]
            [HttpPost]
            public async Task<ActionResult> DefineGameSessionCategories(string gameSessionId) {
                var builder = new ContainerBuilder();
                builder.RegisterType<CategoryApplicationService>().As<ICategoryApplicationService>()
                    .WithParameter("repository", FactoryRepository.Instance.GetRepository<Category, string>());
                var container = builder.Build();


                using (var scope = container.BeginLifetimeScope()) {

                    ICategoryApplicationService gameSessionApplicationService = scope.Resolve<ICategoryApplicationService>();

                    var result = await gameSessionApplicationService.DefineNewCategories(gameSessionId);

                    return Ok(result);
                }
            }
    }
}
