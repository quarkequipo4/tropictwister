﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;

namespace Web_Api.Controllers
{
    [Route("api/defineWinner")]
    [ApiController]
    public class DefineWinnerController : ControllerBase
    {
        [Route("{gameSessionId}/{roundNumber}")]
        [HttpPost]
        public async Task<ActionResult<GameSession>> DefineWinner(string gameSessionId, int roundNumber) {
            var builder = new ContainerBuilder();
            builder.RegisterType<DefineWinnerApplicationService>().As<IDefineWinnerApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope()) {

                IDefineWinnerApplicationService defineWinnerApplicationService = scope.Resolve<IDefineWinnerApplicationService>();

                GameSession result = await defineWinnerApplicationService.DefineWinner(gameSessionId, roundNumber);

                return Ok(result);
            }
        }
    }
}
