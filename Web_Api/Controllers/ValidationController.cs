﻿using Assets.Scripts.DataAccess.Repositories;
using Autofac;
using Web_Api.Contracts;
using Web_Api.Entities;
using Web_Api.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Web_Api.Controllers
{
    [Route("api/validate")]
    [ApiController]
    public class ValidationController : ControllerBase
    {

        [HttpPost]
        [Route("{gameSessionId}")]
        public async Task<ActionResult<Turn>> Validate(string gameSessionId)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ValidationApplicationService>().As<IValidationApplicationService>()
                .WithParameter("repository", FactoryRepository.Instance.GetRepository<GameSession, string>());
            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope()) {

                IValidationApplicationService validationApplicationService = scope.Resolve<IValidationApplicationService>();

                Turn result = await validationApplicationService.Validate(gameSessionId);

                return Ok(result);
            }
        }
    }
}
