﻿
using Web_Api.Entities;
using System;

public class RoundDTO
{
    public int Number { get; set; }

    public static RoundDTO Parse(Round round) {
        RoundDTO newRoundDTO = new RoundDTO();
        newRoundDTO.Number = round.GetNumber();

        return newRoundDTO;
    }
}
