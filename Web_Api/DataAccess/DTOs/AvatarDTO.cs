﻿

public class AvatarDTO
{
    public string Name { get; set; }
    public System.Uri PicturePath { get; set; }

    public AvatarDTO(string name, System.Uri picturePath) {
        this.Name = name;
        this.PicturePath = picturePath;
    }
}