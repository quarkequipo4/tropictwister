﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web_Api.DataAccess.Interfaces
{
    public interface IServiceRepository<T, TId>
    {
        Task<string> Add(T entity);
        Task<string> Update(T entity);
        Task<T> Get(TId id);
        Task<List<T>> GetAll();
        Task Remove(T entity);
    }
}

