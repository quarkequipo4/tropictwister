﻿using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assets.Scripts.DataAccess.Repositories
{
    public class ServiceRepository<T, TId> : IServiceRepository<T, TId> where T : Entity {

        private FirebaseClient reference;

        public ServiceRepository(){
            reference = new FirebaseClient("https://tropictwister-5a171-default-rtdb.firebaseio.com/");
        }

        public async Task<string> Add(T entity) 
        {
            string collectionName = GetCollectionName(typeof(T));

            if (string.IsNullOrEmpty(entity.Id))
            {
                var result = await reference.Child(collectionName).PostAsync(entity);
                if (result != null)
                {
                    entity.Id = result.Key;
                    await reference.Child(collectionName).Child(entity.Id).PutAsync(entity);
                    return "OK";
                }
                return "BadRequest";
                //reference.Child(collectionName).Child(newKey).SetRawJsonValueAsync(MapToJson(entity)); unity code
            }
            else
            {
                await reference.Child(collectionName).Child(entity.Id).PutAsync(entity);
                return "OK";
            }
        }

        public async Task<string> Update(T entity) 
        {
            try
            {
                string collectionName = GetCollectionName(typeof(T));
                await reference.Child(collectionName).Child(entity.Id).PutAsync(entity);
                return "OK";
            }
            catch (Exception)
            {
                return "BadRequest";
                throw;
            }
        }

        public async Task<T> Get(TId id) 
        {
            string collectionName = GetCollectionName(typeof(T));
            var task = reference.Child(collectionName).Child(id.ToString()).OnceSingleAsync<T>();
            await task;
            if (task.IsFaulted) {
                Console.Error.Write(task.Exception);
                throw task.Exception;
            } else if (task.IsCompleted) {
                T entity = task.Result;
                return entity;
                //Debug.Log(snapshot.GetRawJsonValue());        unity code             
            }
            return null;
        }

        public async Task<List<T>> GetAll() {
            string collectionName = GetCollectionName(typeof(T));
            var task = reference.Child(collectionName).OnceAsync<T> ();
            await task;
            if (task.IsFaulted) {
                Console.Error.Write(task.Exception);
                throw task.Exception;
            } else if (task.IsCompleted) {
                List<T> entities = new List<T>(task.Result.Count);
                foreach (var entity in task.Result) {
                    entities.Add(entity.Object);
                }
                return entities;
                //Debug.Log(snapshot.GetRawJsonValue());        unity code             
            }
            return null;
        }

        public Task Remove(T entity)
        {
            throw new NotImplementedException();
        }

        private string GetCollectionName(Type type) {
            if (type == typeof(Category)) {
                return "Categories";
            } else if (type == typeof(WordsPerCategory)) {
                return "WordsPerCategories";
            } else {
                return type.Name + "s";
            }
        }
    }
}
