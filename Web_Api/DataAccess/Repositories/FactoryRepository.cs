﻿
using Web_Api.DataAccess.Interfaces;
using Web_Api.Entities;
using System;
using System.Collections;

namespace Assets.Scripts.DataAccess.Repositories
{
    public class FactoryRepository
    {
        private static FactoryRepository instance;
        public static FactoryRepository Instance {
            get {
                if (instance == null)
                    instance = new FactoryRepository();
                return instance;
            }
        }

        private Hashtable serviceRepositories = new Hashtable();

        public IServiceRepository<T, TId> GetRepository<T, TId>() where T : Entity {
            if (serviceRepositories.ContainsKey(typeof(T))) {
                return (IServiceRepository<T, TId>)serviceRepositories[typeof(T)];
            } else {
                IServiceRepository<T, TId> serviceRepository = new ServiceRepository<T, TId>();
                serviceRepositories.Add(typeof(T), serviceRepository);
                return serviceRepository;
            }
        }

    }
}